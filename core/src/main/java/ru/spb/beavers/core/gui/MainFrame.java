package ru.spb.beavers.core.gui;

import javax.swing.*;

/**
 * Главное окно приложения
 */
public class MainFrame extends JFrame {

    public MainFrame() {
        super("Теория принятия решений");
        this.setResizable(false);
        this.setSize(900, 550);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setContentPane(GUIManager.getContentPane());
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                MainFrame mainFrame = new MainFrame();
                mainFrame.setVisible(true);
            }
        });
    }
}
